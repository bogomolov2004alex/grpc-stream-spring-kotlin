plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}

rootProject.name = "grpc-spring-kotlin-fileupload"

include("proto")
include("server")
include("client")
