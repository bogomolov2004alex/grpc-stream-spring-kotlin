package ru.mirea.server.utils

import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.io.IOException

class DiskFileStorage {

    private val byteArrayOutputStream: ByteArrayOutputStream = ByteArrayOutputStream()

    fun getStream(): ByteArrayOutputStream =
        byteArrayOutputStream

    @Throws(IOException::class)
    fun write(fileName: String) {
        val path = "C:\\Users\\bogom\\IdeaProjects\\grpc-spring-kotlin-fileupload\\output\\"

        FileOutputStream(path + fileName).use {
            byteArrayOutputStream.writeTo(it)
        }
    }

    @Throws(IOException::class)
    fun close() {
        byteArrayOutputStream.close()
    }
}