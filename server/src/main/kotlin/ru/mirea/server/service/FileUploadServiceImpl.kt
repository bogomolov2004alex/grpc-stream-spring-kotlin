package ru.mirea.server.service

import io.grpc.Status
import io.grpc.stub.StreamObserver
import net.devh.boot.grpc.server.service.GrpcService
import ru.mirea.lib.FileUploadRequest
import ru.mirea.lib.FileUploadResponse
import ru.mirea.lib.FileUploadServiceGrpc
import ru.mirea.lib.UploadStatus
import ru.mirea.proto.Constant
import ru.mirea.server.utils.DiskFileStorage
import java.io.IOException

@GrpcService
class FileUploadServiceImpl : FileUploadServiceGrpc.FileUploadServiceImplBase() {

    override fun uploadFile(responseObserver: StreamObserver<FileUploadResponse>): StreamObserver<FileUploadRequest> {
        val fileMetadata = Constant.fileMetadataContext.get()
        val diskFileStorage = DiskFileStorage()

        return object : StreamObserver<FileUploadRequest> {
            override fun onNext(fileUploadRequest: FileUploadRequest) {
                // called when client sends the data
                println("Received ${fileUploadRequest.file.content.size()} length of data")

                try {
                    fileUploadRequest.file.content
                        .writeTo(diskFileStorage.getStream())
                } catch (e: IOException) {
                    responseObserver.onError(
                        Status.INTERNAL.withDescription(
                            "Cannot write data due to ${e.message}"
                        ).asRuntimeException()
                    )
                }
            }

            override fun onError(t: Throwable) {
                // called when client sends the error
                println(t.toString())
            }

            override fun onCompleted() {
                // called when client finished sending the data
                try {
                    val totalBytesReceived = diskFileStorage.getStream().size()
                    if (totalBytesReceived == fileMetadata.contentLength) {
                        diskFileStorage.write(fileMetadata.fileNameWithType)
                        diskFileStorage.close()
                    } else {
                        responseObserver.onError(
                            Status.INTERNAL.withDescription(
                                "Expected ${fileMetadata.contentLength}, but received $totalBytesReceived"
                            ).asRuntimeException()
                        )
                        return
                    }
                } catch (e: IOException) {
                    responseObserver.onError(
                        Status.INTERNAL.withDescription(
                            "Cannot save data due to ${e.message}"
                        ).asRuntimeException()
                    )
                    return
                }

                responseObserver.onNext(
                    FileUploadResponse.newBuilder()
                        .setFileName(fileMetadata.fileNameWithType)
                        .setStatus(UploadStatus.SUCCESS)
                        .build()
                )

                responseObserver.onCompleted()
            }
        }
    }
}