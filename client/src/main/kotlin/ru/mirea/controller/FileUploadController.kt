package ru.mirea.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.mirea.service.FileUploadService

@RestController
@RequestMapping("/api")
class FileUploadController (
    private val fileUploadService: FileUploadService
) {

    @PostMapping("/upload")
    fun uploadFile(@RequestParam("file") multipartFile: MultipartFile): String =
        fileUploadService.uploadFile(multipartFile)
}