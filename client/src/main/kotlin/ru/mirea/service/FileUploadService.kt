package ru.mirea.service

import org.springframework.web.multipart.MultipartFile

interface FileUploadService {

    fun uploadFile(multipartFile: MultipartFile): String
}