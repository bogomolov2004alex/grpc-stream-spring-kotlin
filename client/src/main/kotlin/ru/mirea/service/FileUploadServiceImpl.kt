package ru.mirea.service

import com.google.protobuf.ByteString
import io.grpc.Metadata
import io.grpc.stub.MetadataUtils
import io.grpc.stub.StreamObserver
import net.devh.boot.grpc.client.inject.GrpcClient
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import ru.mirea.lib.*
import ru.mirea.proto.Constant
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.CountDownLatch


@Service
class FileUploadServiceImpl : FileUploadService {

    @GrpcClient(value = "file-upload")
    private lateinit var client: FileUploadServiceGrpc.FileUploadServiceStub

    override fun uploadFile(multipartFile: MultipartFile): String {
        val fileSize: Int?
        val inputStream: InputStream?
        val fileName = multipartFile.originalFilename

        try {
            fileSize = multipartFile.bytes.size
            inputStream = multipartFile.inputStream

        } catch (e: IOException) {
            e.printStackTrace()
            return "Unable to extract file info"
        }

        val response = StringBuilder()
        val countDownLatch = CountDownLatch(1)

        val metadata = Metadata()
        metadata.put(
            Constant.fileMetadataKey,
            FileMetadata
                .newBuilder()
                .setContentLength(fileSize)
                .setFileNameWithType(fileName)
                .build()
                .toByteArray()
        )

        val fileUploadRequestStreamObserver = client
            .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata))
            .uploadFile(
                object : StreamObserver<FileUploadResponse> {

                    override fun onNext(fileUploadResponse: FileUploadResponse) {
                        // called when server sends the response
                        response.append(fileUploadResponse.status)
                    }

                    override fun onError(t: Throwable) {
                        // called when server sends any error
                        response.append(UploadStatus.FAIL)
                        t.printStackTrace()
                        countDownLatch.countDown()
                    }

                    override fun onCompleted() {
                        // called when server finishes serving the request
                        countDownLatch.countDown()
                    }
                },
            )

        try {
            val fiveKB = ByteArray(5120)
            var length: Int

            while ((inputStream.read(fiveKB).also { length = it }) > 0) {
                println("Sending $length length of data to server")

                val request = FileUploadRequest
                    .newBuilder()
                    .setFile(
                        File.newBuilder()
                            .setContent(
                                ByteString.copyFrom(fiveKB, 0, length)
                            ).build()
                    ).build()

                // sending the request that contains chunked data of file
                fileUploadRequestStreamObserver.onNext(request)
            }

            inputStream.close()
            fileUploadRequestStreamObserver.onCompleted()
            countDownLatch.await()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            response.append(UploadStatus.FAIL)
        }

        return response.toString()
    }
}