plugins {
    id("org.springframework.boot")

    kotlin("jvm")
    kotlin("plugin.spring")
}

dependencies {
    implementation(project(":proto"))

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("net.devh:grpc-client-spring-boot-starter:2.15.0.RELEASE")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}
